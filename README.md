# dotfiles
You know what it is

# Usage

Summarized and customized from [this](https://www.atlassian.com/git/tutorials/dotfiles) guide. 

Run this installer:

```shell
curl -Lks https://dotfiles.steele.co/dotfile-setup | /bin/bash
```

